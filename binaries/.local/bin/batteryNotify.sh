#!/bin/bash

# Idea: Llegir capacity i status cada 2 minuts. 
# Quatre possibles situacions: 
# 1. Status discharging: Capacity baixa. Si està per sota de 20%, enviar avís.
# 2. Status discharging: Capacity puja. Enviar avís.
#   A partir d'aquí, fer servir una baixada de capacitat relativa.
# 3. Status charging. Qualsevol capacity.


capacity_path="/sys/class/power_supply/BAT1/capacity"
status_path="/sys/class/power_supply/BAT1/status"
last_known_capacity=$(cat $capacity_path)
# notify-send $(cat $capacity_path)


while [ true ]; do
    notify-send "$last_known_capacity" 
    $(sleep 5s)
done
