#!/bin/sh

echo autospawn = no > $HOME/.config/pulse/client.conf
pulseaudio --kill
read -p "Press enter to enable pulseaudio again."
rm $HOME/.config/pulse/client.conf
pulseaudio --start
