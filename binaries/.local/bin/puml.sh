#!/bin/bash

# 1. Creates image of plantuml, and opens that image on new process
# 2. watches for changes on that same plantuml, and executes plantuml
# 3. Starts vim, and when vim stops, everything closes


#$1 = the file

filename=$(basename -- "$1")
#imgExtension="png"

#echo "basename $(basename -- "$1")"
#echo "extension="${filename##*.}""
#echo "realpath $1"
#echo "base name without extension: ${filename%.*}"
#echo "file name with another extension ${filename%.*}.png"

plantuml "$1"
sxiv "${filename%.*}.png" & disown
st nvim "$1" & disown
echo "$1" | entr plantuml "$1"
