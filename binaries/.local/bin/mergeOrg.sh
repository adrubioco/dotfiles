#!/bin/bash

# For every org file in the specified first argument directory,
# make it's outlines go down one level, and put the file name in it's
# first level. Then, copy the modified stream to the second argument (a file).

#function addToFile

for file in "$1"/*.org; do
    filename=$(basename -- "$file")
    # extension="${filename##*.}"
    # filename="${filename%.*}"
    sed "s/^*/**/g" "$file" | sed "1 i \* $filename" >> "$1"/merged.org
done
