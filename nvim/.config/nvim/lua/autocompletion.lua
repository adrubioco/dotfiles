vim.cmd([[
" autocompletion
inoremap (;       ();<left><left>
inoremap [;       [];<left><left>
inoremap {<cr>    {<cr>}<esc>O
inoremap {;<cr>   {<cr>};<esc>O
inoremap sout<cr> system.out.println("");<left><left><left>
]])
