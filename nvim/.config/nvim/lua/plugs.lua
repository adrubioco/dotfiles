vim.cmd([[

" Install VimPlug if not installed
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.config/nvim/plugged')

" Base
Plug 'justinmk/vim-dirvish'

" Libraries
Plug 'inkarkat/vim-ingo-library'

" Beautify vim
Plug 'nathanaelkane/vim-indent-guides'
Plug 'psliwka/vim-smoothie'
Plug 'lifepillar/vim-solarized8'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Actions & textObjects
Plug 'wellle/targets.vim'
Plug 'michaeljsmith/vim-indent-object'
Plug 'tpope/vim-surround'
Plug 'machakann/vim-highlightedyank'
Plug 'tpope/vim-commentary'
Plug 'vim-scripts/ReplaceWithRegister'
Plug 'junegunn/vim-easy-align'

" Automation
Plug 'mattn/emmet-vim'

" Syntax and lang specific
Plug 'prettier/vim-prettier'
Plug 'udalov/kotlin-vim'
Plug 'neovimhaskell/haskell-vim'
Plug 'vim-scripts/indentpython'
" Plug 'lervag/vimtex'
Plug 'aklt/plantuml-syntax'
Plug 'axvr/org.vim'

" Plugins
Plug 'tmhedberg/SimpylFold'
Plug 'junegunn/fzf'
Plug 'jbyuki/instant.nvim'

" Plug 'ying17zi/vim-live-latex-preview.git'
" A Vim Plugin for Lively Previewing LaTeX PDF Output
" Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }

" Plug 'Olical/vim-scheme', { 'for': 'scheme', 'on': 'SchemeConnect' }

" You'll need vim-sexp too for selecting forms.
" Plug 'guns/vim-sexp'
" And while you're here, tpope's bindings make vim-sexp a little nicer to use.
" Plug 'tpope/vim-sexp-mappings-for-regular-people'

" Initialize plugin system
call plug#end()

]])
