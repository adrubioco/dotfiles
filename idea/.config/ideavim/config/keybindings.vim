let mapleader =" "

" VIM CONFIG
    nnoremap <leader>ce :e ~/.config/ideavim/ideavimrc<CR>
    nnoremap <leader>cr :action IdeaVim.ReloadVimRc.reload<CR>

~ CODE NAVIGATION
    nnoremap <leader>su :action FindUsages<CR>
    nnoremap gd :action GotoDeclaration<CR>
    nnoremap <C-o> :action Back<CR>
    nnoremap <C-i> :action Forward<CR>

"" REFACTORING
    nnoremap rr :action RenameElement<CR>

"" INFO
nnoremap <C-p> :action ParameterInfo<CR>

nnoremap <C-t> :NERDTreeToggle<CR> " Plugin settings

" Shortcutting tabs
    map <leader>tc :tabclose<cr>
    map <leader>tn :tabedit<cr>
    map <leader>tl :tabn<cr>
    map <leader>th :tabp<cr>

" Buffer operations
    nnoremap <leader>bn :bnext<cr>
    nnoremap <leader>bp :bprevious<cr>
    nnoremap <leader>bs :w<cr>
    nnoremap <leader>bb :ls<cr>
    nnoremap <leader>fs :w<cr>
    nnoremap <leader>bd :bdelete<cr>
    nnoremap <leader>qq :quit<cr>

" Files
    nnoremap <leader>fs :w<cr>

" Splits
    nnoremap <C-h> <C-W>h
    nnoremap <C-j> <C-w>j
    nnoremap <C-k> <C-w>k
    nnoremap <C-l> <C-w>l
    nnoremap <leader>wh <C-w>h
    nnoremap <leader>wj <C-w>j
    nnoremap <leader>wk <C-w>k
    nnoremap <leader>wl <C-w>l
    nnoremap <leader>wn <C-w>n
    nnoremap <leader>wd :wq<cr>
    nnoremap <leader>wv :vs<cr>
    nnoremap <leader>ws :sp<cr>
