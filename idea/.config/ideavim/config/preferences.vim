set encoding=utf-8
set number
syntax enable
set clipboard=unnamed
set clipboard+=ideaput
set cursorline
set go=a
set mouse=a
set nohlsearch
set wildmenu
set breakindent
set formatoptions=l
set splitbelow
set splitright
set hidden
"set showbreak=....
set ideajoin " Use Idea to join lines smartly

" sets system clipboard as the unnamed clipboard
set clipboard+=unnamed

set visualbell
set idearefactormode=keep

" Splits and tabs
    set splitbelow
    set splitright

" Indenting
    set breakindent
    set showbreak=....

" Search
    set hlsearch
