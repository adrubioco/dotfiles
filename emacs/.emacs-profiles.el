(("bare" . ((user-emacs-directory . "~/.config/emacs/bare")))
 ("default" . ((user-emacs-directory . "~/.config/emacs/default")))
 ("prelude" . ((user-emacs-directory . "~/.config/emacs/prelude")))
 ("elegant" . ((user-emacs-directory . "~/.config/emacs/elegant")))
 ("spacemacs" . ((user-emacs-directory . "~/.config/emacs/emacs-spacemacs.d")
                 (env . (("SPACEMACSDIR" . "~/.config/emacs/spacemacs.d")))))
 ;; user-emacs-directory is where doom is installed
 ;; The DOOMDIR is where our config files are located
 ("doom" . ((user-emacs-directory . "~/.config/emacs/emacs-doom.d")
            (env . (("DOOMDIR" . "~/.config/emacs/doom.d"))))))
