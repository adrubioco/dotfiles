import Text.Pandoc.JSON

main = toJSONFilter noAttrs

-- Type signature (convert a block into a slightly modified block)
noAttrs :: Block -> Block
-- Header constructors have the form:
--  Header Int Attr [Inline]
-- _ means we ignore the attribute (Attr), since we're discarding it, anyway
-- nullAttr is, as the name suggests, an attribute containing no info
noAttrs (Header n _ i) = Header n nullAttr i
-- Div constructors have the form:
--  Div Attr [Block]
noAttrs (Div _ b) = Div nullAttr b
-- for completeness could also deal with 'CodeBlock's, which can also
-- have an attribute — left as an exercise for the reader.
--
-- we need a fallthrough
noAttrs b = b

-- SOURCE: https://emacs.stackexchange.com/questions/54400/export-a-docx-file-to-org-using-pandoc-but-without-the-property-drawers
-- You must compile the file with:
-- _ ghc noattrs-filter.hs
-- and run your conversion with:
-- _ pandoc -s --filter ./noattrs-filter test.docx -o test.org
-- In order to compile the pandoc filter, you need to have the relevant libraries. For instance, on Ubuntu, you'd need the libghc-pandoc-types-dev package (sudo apt-get install libghc-pandoc-types-dev). More generally, you could also try installing via cabal (cabal install pandoc).

-- Relevant documentation:
-- _ https://hackage.haskell.org/package/pandoc-types-1.20/docs/Text-Pandoc-JSON.html
-- _ https://hackage.haskell.org/package/pandoc-types-1.20/docs/Text-Pandoc-Definition.html#t:Block
