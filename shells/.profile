eval $(ssh-agent)

# Default programs
export VISUDO_EDITOR="nvim"
export SUDO_EDITOR="nvim"
export EDITOR="nvim"
export VISUAL="emacs"
export PAGER="less"
export TERMINAL="st"
export READER="zathura"
export FILE="lf"
export BROWSER="firefox"

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.local/cache"
export XDG_RUNTIME_DIR="/tmp/runtime-adria"

# Development packages
export GOPATH="$XDG_CACHE_HOME/go"
export GOBIN="$HOME/.local/bin"

export INSTALL4J_JAVA_HOME="$HOME/.local/lib/jvm/jdk-16.0.2+7"
export BIB="$HOME/Documents/LaTeX/uni.bib"

export ANDROID_HOME="$XDG_DATA_HOME/Android/Sdk"
export ANDROID_USER_HOME="$XDG_CONFIG_HOME/Android/Sdk"

export SUDO_ASKPASS="$HOME/.local/bin/dmenupass"

export QT_QPA_PLATFORMTHEME="qt5ct" #Fixes Qt apps, somehow

# This variable cannot be declared on the .xprofile file, but is needed for Xorg to work.
# If you set $HOME as read-only, Xorg cannot create .Xauthority, and won't be able to start!
export XAUTHORITY="/tmp/Xauthority"

PATH="$HOME/.config/emacs/emacs-doom.d/bin:$PATH"
PATH="$HOME.vim/bundle/vim-live-latex-preview/bin:$PATH"
PATH="$HOME/.local/bin:$PATH"
PATH="$HOME/.local/sbin:$PATH"
PATH="$HOME/.local/kotlin-language-server/bin:$PATH"
export PATH

# FZF colors
export FZF_DEFAULT_OPTS='
  --color=bg+:#073642,bg:#002b36,spinner:#719e07,hl:#586e75
  --color=fg:#839496,header:#586e75,info:#cb4b16,pointer:#719e07
  --color=marker:#719e07,fg+:#839496,prompt:#719e07,hl+:#719e07
'
# less/man colors
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"

# fix for some tmux color management issues
[ "$TERM" = "st" ] && TERM="st-256color"

# Start graphical server if Xorg is not already running.
[ "$(tty)" = "/dev/tty1" ] && ! pgrep -x Xorg >/dev/null && exec startx
