# ====================================
# Aliases
# ====================================

#alias cdp="cd $(xclip -o)"

# Package management
alias e="nvim"

# System
alias homeon="chmod +w $HOME"
alias homeoff="chmod -w $HOME"

# Programs
alias ymusic="rlwrap youtube-viewer --video-player=mpv -n"
alias emcs="emacsclient -create-frame --alternate-editor="" & disown && exit"
alias puml="~/Documentos/scripts/puml.sh"
alias area-screenshot='ffcast -s trim png'
alias grabarnoaudio='ffmpeg -f v4l2 -video_size 640x480 -i /dev/video0 -c:v libx264 -preset ultrafast'

alias vol="pamixer --allow-boost --set-volume"

alias tenes="trans en:es"
alias tesen="trans es:en"

alias npm-install="npm --prefix ~/.local -g install"
alias lispstyle="astyle --style=lisp --indent=spaces=2"
alias list-available-kernels="xbps-query --regex -Rs '^linux[0-9.]+-[0-9._]+'"
